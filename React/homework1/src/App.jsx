import React from 'react';
import './App.css';
import {Button} from "./Components/Button"
import {Modal} from "./Components/Modal";


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal1: false,
            modal2: false
        };
    }

    firstModal = () => {
        this.setState({...this.state, modal1: true})
    }

    secondModal = () => {
        this.setState({...this.state, modal2: true})
    }

    closeFirstModal = () => {
        this.setState({...this.state, modal1: false})
    }
    closeSecondModal = () => {
        this.setState({...this.state, modal2: false})

    }
    closeBothModals = () => {
        this.setState({...this.state, modal1: false, modal2: false})

    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <p>
                        <Button backgroundColor="#b3382c" buttonClassName="Button" buttonText="Open first modal"
                                callback={this.firstModal}/>
                        <Button backgroundColor="#b3382c" buttonClassName="Button" buttonText="Open second modal"
                                callback={this.secondModal}/>
                    </p>
                </header>
                <Modal oneMore={this.closeBothModals} btnColor1="#d44637" btnColor2="#d44637" closeButton={true} display={this.state.modal1}
                       modalTitle="Do you want to delete this file"
                       modalContent="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
                       modalAction={() => {
                           console.log("Hi there!")
                       }} modalCloseAction={this.closeFirstModal} modalClassName="modal first-modal"/>
                <Modal oneMore={this.closeBothModals} btnColor1="#E53EFF" btnColor2="#FFF000" closeButton={false} display={this.state.modal2}
                       modalTitle="Here we got another one" modalContent="Lorem ipsum dolor sit amet, consectetur adip"
                       modalAction={() => {
                           console.log("Hi there!")
                       }} modalCloseAction={this.closeSecondModal} modalClassName="modal second-modal"/>
            </div>
        );
    }
}

export default App;

import React from 'react';
import {Button} from "./Button"

import "./Modal.scss"

class Modal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {
            modalTitle,
            modalContent,
            modalAction,
            modalClassName,
            display,
            closeButton,
            modalCloseAction,
            btnColor1,
            btnColor2,
            oneMore
        } = this.props;

        function checkStatus() {
            if (!display) {
                return "none"
            } else {
                return ""
            }
        }

        return (
            <div className="modal__background" style={{display: checkStatus()}} onClick={(e) => {
                if (!e.target.closest(".modal")) {
                    oneMore();
                }
            }}>
                <article className={modalClassName}>
                    <header className="modalTitle">{modalTitle}</header>
                    <div className="modal__content">{modalContent}</div>
                    {closeButton
                        ? <div className="modal__cross" onClick={modalCloseAction}>x</div>
                        : ""
                    }
                    <Button backgroundColor={btnColor1} buttonClassName="Button Button__ok" buttonText="Ok"
                            callback={modalAction}/>
                    <Button backgroundColor={btnColor2} buttonClassName="Button Button__cancel" buttonText="Cancel"
                            callback={modalCloseAction}/>
                </article>
            </div>
        )
    }
}

export {Modal}
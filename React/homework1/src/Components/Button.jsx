import React from 'react';

import "./Button.scss";

class Button extends React.Component {
    render() {
        let {buttonText, buttonClassName, callback, backgroundColor} = this.props;

        return (
            <button className={buttonClassName} onClick={callback} style={{backgroundColor: backgroundColor}}>{buttonText}</button>
        )
    }
}

export {Button}
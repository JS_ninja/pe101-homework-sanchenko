const ROOT = document.querySelector("#root");

const usersURL = "https://ajax.test-danit.com/api/json/users";
const postsURL = "https://ajax.test-danit.com/api/json/posts";
const postDeleteURL = "https://ajax.test-danit.com/api/json/posts/";

class Request {
    static GET(url) {
        return fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => response.json());
    }

    static DELETE(url, id) {
        return fetch(url + id, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => {
            return response.status
        });
    }
}

class Card {
    constructor(post) {
        this.id = post.id;
        this.title = post.title;
        this.body = post.body;
        this.name = post.currentUser.name;
        this.username = post.currentUser.username;
        this.email = post.currentUser.email;
    }

    static Delete(id) {
        ROOT.querySelector(`[data-post-id="${id}"]`).remove();
    }

    render() {
        const article = document.createElement("article");
        article.className = "card";
        article.dataset.postId = this.id;
        const header = document.createElement("header");
        article.append(header);
        const title = document.createElement("h2");
        title.className = "card-title";
        title.textContent = this.title;
        header.append(title);
        const subtitle = document.createElement("div");
        subtitle.className = "card-subtitle";
        subtitle.innerHTML = `<p><span class="card-username">@${this.username}</span> (${this.name}) - ${this.email}</p>`;
        header.append(subtitle);
        const text = document.createElement("p");
        text.className = "card-text";
        text.textContent = this.body;
        article.append(text);
        const delButton = document.createElement("button");
        delButton.className = "card-del-btn";
        delButton.title = "Delete this card";
        delButton.textContent = "❌";
        delButton.addEventListener("click", (e) => {
            const id = e.target.closest(".card").dataset.postId;
            Request.DELETE("https://ajax.test-danit.com/api/json/posts/", id)
                .then((status) => {
                    if (status === 200) {
                        Card.Delete(id);
                    }
                })
        })
        article.append(delButton);

        return article;
    }
}

async function initialize() {
    return {posts: await Request.GET(postsURL), users: await Request.GET(usersURL)}
}

function collectPostsData({posts, users}) {
    posts.forEach(post => {

    })
    const cards = posts.map(post => {
        post.currentUser = users[users.findIndex((user) => {
            if (post.userId === user.id) {
                return true
            }
        })];
        return new Card(post)
    })
    return cards
}

function renderPosts(cards) {
    cards.forEach(card => {
        root.append(card.render())
    })
}

initialize()
    .then(collectPostsData)
    .then(renderPosts)
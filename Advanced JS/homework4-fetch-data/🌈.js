const ROOT = document.querySelector("#root");
const filmsURL = "https://ajax.test-danit.com/api/swapi/films"

function makeRequest(URL) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", URL);
        xhr.send();

        xhr.onload = () => {
            resolve(xhr.response);
        };

        xhr.onerror = (e) => {
            reject(e);
        };
    })
}

function openJSON(json) {
    return JSON.parse(json)
}

function renderFilmsData(films) {
    const ul = document.createElement("ul");
    ul.className = "films-list";
    root.append(ul);

    class FilmListItem {
        constructor({episodeId, name, openingCrawl, id}) {
            this.episodeId = episodeId;
            this.name = name;
            this.openingCrawl = openingCrawl;
            this.id = id;
        }

        render() {
            const li = document.createElement("li");
            li.className = "film-list__item";
            li.dataset.filmId = this.id;
            li.innerHTML = `<p>${this.episodeId}) ${this.name}:</p><p>${this.openingCrawl}</p><p class="characters">Characters:\n</p><span class="loader">🤡</span>`
            ul.append(li);
        }
    }

    films.forEach(film => {
        let li = new FilmListItem(film);
        li.render();
    })

    return films;

}

function getCharacters(films) {
    films.forEach(({id, characters}) => {
        let p = document.querySelector(`[data-film-id='${id}'] > .characters`);
        let string = "";
        // characters.forEach((c) => {})
        for (let i = 0; i < characters.length; i++) {
            let c = characters[i];
            makeRequest(c)
                .then(openJSON)
                .then((charData) => {
                    string += renderCharacter(charData);
                    string += ", ";
                })
                .then(() => {
                    if (i === characters.length - 1) {
                        document.querySelector(`[data-film-id='${id}'] > .loader`).remove();
                        p.textContent += string;
                    }
                });

        }
    })
}

function renderCharacter({name}) {
    return name
}

makeRequest(filmsURL)
    .then(openJSON)
    .then(renderFilmsData)
    .then(getCharacters)
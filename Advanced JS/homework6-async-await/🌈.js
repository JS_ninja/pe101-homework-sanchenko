const ROOT = document.querySelector("#root");
const BUTTON = document.querySelector("#button");

const HACKER = document.querySelector("#hacker");

const ipURL = "https://api.ipify.org/?format=json";
const geoURL = "http://ip-api.com/json/";

function writeText(text) {
    const p = document.createElement("p");
    ROOT.append(p);
    let i = 0;

    function writeLetter() {
        p.textContent += text.split("")[i++];
        if (text.split("").length > i) {
            setTimeout(writeLetter, Math.floor(Math.random() * (150 - 25)) + 25);
        }
    }

    writeLetter()
}

class Request {
    static async GET(url) {
        return fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => response.json());
    }
}

BUTTON.addEventListener("click", async () => {
    BUTTON.remove();
    writeText("I'll find you!");
    setTimeout(async ()=>{
        let {ip} = await Request.GET(ipURL);
        writeText(`Your ip is: ${ip}`);
        let {country, city, zip} = await Request.GET(geoURL+ip);
        writeText(`You located in ${country}, ${city} (ZIP-code: ${zip})`);
        HACKER.style.display = "flex";
        setTimeout(()=>{
            HACKER.style.marginTop = "40vh";
        },5000)
    },500)

})
